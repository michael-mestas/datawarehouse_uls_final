from django.conf.urls import url

from dosevidencia.views import (
    cosine_adjusted_view, k_vecinos_view, prediction_view,
    generate_deviations_view, slope_one_view)

app_name = 'evdos'
urlpatterns = [
    url(r'^prediccionitem/(\w+)/(\d+)/(\d+)/', prediction_view),

    url(r'^cosenoajustado/(\d+)/(\d+)/', cosine_adjusted_view),

    url(r'slopeone/(\d+)/(\d+)/', slope_one_view),

    url(r'k/(\w+)/(\d+)/(\d+)/', k_vecinos_view),

    url(r'deviations/', generate_deviations_view)
]