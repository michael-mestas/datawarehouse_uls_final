import time

from django.shortcuts import render

from .utils import cosine_adjusted, predecir, generate_deviations, slope_one
from unoevidencia.models import Item, User, Affinity


def prediction_view(request, medida, user_id, item_id):
    context = {
        'resultado': predecir(user_id, item_id, medida),
        'nombre_usuario': User.objects.get(id=user_id).name,
        'nombre_item': Item.objects.get(id=item_id).name
    }
    return render(request, 'prediccion_usuarios.html', context)


def cosine_adjusted_view(request, item_1_id, item_2_id):

    start = time.time()
    context = {
        'nombre_item_1': Item.objects.get(id=int(item_1_id)).name,
        'nombre_item_2': Item.objects.get(id=int(item_2_id)).name,
        'resultado': cosine_adjusted(item_1_id, item_2_id)
    }
    end = time.time()
    context['process_time'] = end - start
    return render(request, 'cosine_ajustado.html', context)


def slope_one_view(request, user_id, item_id):

    start = time.time()
    user = User.objects.get(id=user_id)
    item = Item.objects.get(id=item_id)
    context = {
        'user_name': user.name,
        'item_name': item.name,
        'predicted_or_score': 'Predicted score'
    }
    if Affinity.objects.filter(item=item, user=user).exists():
        context['predicted_or_score'] = 'Score'
        context['score'] = Affinity.objects.get(item=item, user=user).rating
        return render(request, 'slope_one.html', context)
    context['score'] = slope_one(user_id, item_id)
    end = time.time()
    context['process_time'] = end - start

    return render(request, 'slope_one.html', context)




def k_vecinos_view(request, medida, k, item_id):

    start = time.time()
    medidas_similitud={
        'cosenoajustado': cosine_adjusted,
    }
    target_person = Item.objects.get(id=item_id)
    resultados = []
    for item in Item.objects.exclude(id=item_id).iterator():
        resultados.append(
            (
                item.name,
                medidas_similitud[medida](target_person.id, item.id)
            )
        )

    resultados.sort(key=lambda x: x[1],
        reverse=True if medida != '' else False
    )
    context = {
        'resultados': resultados[:int(k)]
    }
    end = time.time()
    context['process_time'] = end - start

    return render(request, 'k_vecinos_item.html', context)


def generate_deviations_view(request):
    if request.method == 'GET':
        start = time.time()
        generate_deviations()
        end = time.time()
        context = {}
        context['process_time'] = end - start

        return render(request, 'deviation.html', context)

def home(request):
    return render(request, 'home/index.html', {})