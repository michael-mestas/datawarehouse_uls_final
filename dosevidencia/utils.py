from math import sqrt

from unoevidencia.models import User, Affinity, Item, Deviation
from unoevidencia.utils import similarity_users


def predecir(user_id, item_id, medida):

    target_user = User.objects.filter(id=user_id).first()
    target_item = Item.objects.filter(id=item_id).first()

    affinity = Affinity.objects.filter(user=target_user, item=target_item)
    if affinity.exists():
        return affinity.first().rating

    filtered_users = []
    for user in User.objects.exclude(id=user_id):
        current_affinity = Affinity.objects.filter(user=user, item=target_item)
        if current_affinity.exists():
            filtered_users.append(user)

    user_similarities = similarity_users(user_id, filtered_users, medida)

    total_sum = 0
    for user in filtered_users:
        total_sum += user_similarities[user.slug]

    total = 0
    for user in filtered_users:
        total += Affinity.objects.get(item=target_item, user=user).rating * (
            user_similarities[user.slug] / total_sum)

    return total


def get_promedio(pts):
    total = 0
    for punt in pts:
        total += punt.rating
    return total / pts.count()


def cosine_adjusted(item_1_id, item_2_id):
    item_1 = Item.objects.get(id=item_1_id)
    item_2 = Item.objects.get(id=item_2_id)
    qualified_users = []
    for user in User.objects.all():
        if Affinity.objects.filter(
                user=user, item__in=[item_1, item_2]).count() > 1:
            qualified_users.append(user)
        var_1 = 0
        var_2 = 0
        var_3 = 0
    for user in qualified_users:
        promedio = get_promedio(Affinity.objects.filter(user=user))
        ptje_a = Affinity.objects.get(user=user,
                                      item=item_1).rating
        ptje_b = Affinity.objects.get(user=user,
                                      item=item_2).rating

        calc_a = ptje_a - promedio
        calc_b = ptje_b - promedio
        var_1 += calc_a * calc_b
        var_2 += calc_a ** 2
        var_3 += calc_b ** 2
    return var_1 / (sqrt(var_2) * sqrt(var_3)) if (
      sqrt(var_2) * sqrt(var_3)) != 0 else 0


def deviation(item_1, item_2):

    sub_totales = []
    total_objects = 0
    for user in User.objects.all():
        rating_1 = Affinity.objects.filter(user=user, item=item_1)
        rating_2 = Affinity.objects.filter(user=user, item=item_2)
        if rating_1.exists() and rating_2.exists():
            sub_totales.append(rating_1.first().rating
                               - rating_2.first().rating)
            total_objects += 1
    total = 0
    for sub_total in sub_totales:
        total += sub_total / total_objects

    return total


def generate_deviations():

    for item_1 in Item.objects.all():
        for item_2 in Item.objects.exclude(pk=item_1.pk):
            score = deviation(item_1, item_2)
            Deviation.objects.update_or_create(
                item_1=item_1, item_2=item_2, score=score)


def cardinality(item_1, item_2):

    queryset = Affinity.objects.filter(item__in=[item_1, item_2])
    user_numer = 0
    for user in User.objects.all():
        user_numer += 1 if queryset.filter(user=user).count() > 1 else 0
    return user_numer


def slope_one(user_id, item_id):

    user = User.objects.get(id=user_id)
    target_item = Item.objects.get(id=item_id)
    nominador = 0
    denominador = 0
    for item in Item.objects.all():
        affinity = Affinity.objects.filter(item=item, user=user)
        if affinity.exists():
            cardinalidad = cardinality(target_item, item)
            nominador += (
                (Deviation.objects.get(item_1=target_item, item_2=item).score
                 + Affinity.objects.get(item=item, user=user).rating)
                * cardinalidad)
            denominador += cardinalidad
    return nominador / denominador
