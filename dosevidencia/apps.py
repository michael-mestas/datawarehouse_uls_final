from django.apps import AppConfig


class DosevidenciaConfig(AppConfig):
    name = 'dosevidencia'
