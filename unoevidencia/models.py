from django.db import models


class User(models.Model):

    name = models.CharField(max_length=32)
    slug = models.CharField(max_length=128, unique=True, null=True, blank=True)
    is_centroid = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Item(models.Model):

    name = models.CharField(max_length=32)
    slug = models.CharField(max_length=128, unique=True, null=True, blank=True)

    def __str__(self):
        return self.name


class Affinity(models.Model):

    user = models.ForeignKey(User)
    item = models.ForeignKey(Item)
    rating = models.FloatField(null=True, blank=True)

    def __str__(self):
        return "{var_1} + {var_2}".format(var_1=self.user, var_2=self.item)


class Deviation(models.Model):

    item_1 = models.ForeignKey(Item, related_name='item_1')
    item_2 = models.ForeignKey(Item, related_name='item_2')
    score = models.FloatField()

    class Meta:
        unique_together = ('item_1', 'item_2')

    def __str__(self):
        return "{}, {}".format(self.item_1, self.item_2)

