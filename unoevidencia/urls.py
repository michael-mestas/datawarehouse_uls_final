from unoevidencia.views import (
    manhattan_view, euclidean_view, k_vecinos_view, sim_coseno_view,
    pearson_view)

from django.conf.urls import url


urlpatterns = [
    url(r'^manhattan/(\d+)/(\d+)/', manhattan_view),
    url(r'^euclidean/(\d+)/(\d+)/', euclidean_view),
    url(r'^simcos/(\d+)/(\d+)/', sim_coseno_view),
    url(r'^pearson/(\d+)/(\d+)/', pearson_view),

    url(r'k/(\w+)/(\d+)/(\d+)/', k_vecinos_view),
]