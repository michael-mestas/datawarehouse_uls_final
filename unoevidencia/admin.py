from django.contrib import admin
from unoevidencia.models import User, Item, Affinity, Deviation


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')


class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')


class AffinityAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'item', 'rating')


class DeviationAdmin(admin.ModelAdmin):
    list_display = ('item_1', 'item_2', 'score')

admin.site.register(User, UserAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Affinity, AffinityAdmin)
admin.site.register(Deviation, DeviationAdmin)
