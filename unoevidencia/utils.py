from math import sqrt

from unoevidencia.models import User, Item, Affinity


def existe_afinidades(person_1, person_2, item_actual):
    return Affinity.objects.filter(
        item=item_actual, user=person_1).exists() and Affinity.objects.filter(
        item=item_actual, user=person_2).exists()


def manhattan(person_1_var, person_2_var):
    """
    3.5-2 + 2-3.5 + 5-2 + 1.5-3.5 + 2-3
    1.5 + 1.5 + 3 + 2 +1
    9
    """

    person_1 = User.objects.filter(id=person_1_var).first()
    person_2 = User.objects.filter(id=person_2_var).first()

    resultado = 0
    for item_actual in Item.objects.all().iterator():

        if existe_afinidades(person_1, person_2, item_actual):
            resultado += abs(
                Affinity.objects.filter(
                    item=item_actual, user=person_1).first().rating
                - Affinity.objects.filter(
                    item=item_actual, user=person_2).first().rating
            )
    return resultado


def euclidean(person_1_var, person_2_var):

    person_1 = User.objects.filter(id=person_1_var).first()
    person_2 = User.objects.filter(id=person_2_var).first()
    resultado = 0
    for item_actual in Item.objects.all().iterator():
        if existe_afinidades(person_1, person_2, item_actual):
            resultado += (
                (
                    Affinity.objects.filter(
                        item=item_actual, user=person_1).first().rating
                    - Affinity.objects.filter(
                        item=item_actual, user=person_2).first().rating)
                ** 2)

    return sqrt(resultado)


def sim_coseno(person_1_var, person_2_var):
    """
    ((5*3) + (2*0) + (3*5) + (5*4) + (4*2.5) + (5*3) )/
    (sqrt(5^2 + 2^2 + 3^2 + 5^2 + 4^2 + 5^2) * (3^2 + 5^2 + 4^2 + 2.5^2 + 3^2))

    15 + 0 + 15 + 20 + 10 + 15 /
    sqrt(25 + 4 +9 + 25 +16 + 25) * sqrt(9 + 25 + 16 + 6.25 + 9)

    75 / sqrt(104) * sqrt(65.25)
    75 / (10.19 * 8.07)

    0.91
    """
    person_1 = User.objects.filter(id=person_1_var).first()
    person_2 = User.objects.filter(id=person_2_var).first()
    var1 = 0
    var2 = 0
    var3 = 0
    for item_actual in Item.objects.all().iterator():
        afinidad_x = Affinity.objects.filter(user=person_1, item=item_actual)
        afinidad_y = Affinity.objects.filter(user=person_2, item=item_actual)
        if afinidad_x.exists() or afinidad_y.exists():

            rating_af_1 = afinidad_x.first().rating if afinidad_x.exists() else 0
            rating_af_2 = afinidad_y.first().rating if afinidad_y.exists() else 0
            var1 += rating_af_1 * rating_af_2
            var2 += rating_af_1 ** 2
            var3 += rating_af_2 ** 2

    return var1 / (sqrt(var2) * sqrt(var3))


def pearson(person_1_var, person_2_var):
    """
    angelica - harley
    2-4 4.5-4 2.5-4 2-1
    (2*4 + 4.5*4 + 2.5*4 + 2*1) - (2 + 4.5 + 2.5 + 2) (4 + 4 +4 + 1) /
    (2^2 + 4.5^2 + 2.5^2 + 2^2) - (2 + 4.5 + 2.5 + 2)^2/4 *
    (4^2 + 4^2 +4^2 + 1^2) - (4 + 4 +4 + 1)^2/4

    (8+18+10+1) - (11)*(13)/
    (4+20.25+6.25+4) - (11)^2 /4 *
    (16+16+16+1) - (13)^2/4

    37 - 143 / sqrt(34.5 - 121/4) * sqrt(49 - 169/4)
    106 / (2.0 * 2.59)
    """
    person_1 = User.objects.filter(id=person_1_var).first()
    person_2 = User.objects.filter(id=person_2_var).first()
    var1 = 0 # sum xi *yi
    var2 = 0 # sum xi's
    var3 = 0 # sum yi's
    var4 = 0 # sum xi's ^ 2
    var5 = 0 # sum yi's ^ 2
    n = 0
    for item_actual in Item.objects.all().iterator():
        if existe_afinidades(person_1, person_2, item_actual):
            x = Affinity.objects.get(item=item_actual, user=person_1).rating
            y = Affinity.objects.get(item=item_actual, user=person_2).rating
            var1 += x * y
            var2 += x
            var3 += y
            var4 += x ** 2
            var5 += y ** 2
            n += 1

    if sqrt(var4 - ((var2 ** 2) / n)) * sqrt(var5 - ((var3 ** 2) / n)) == 0:
        return 0.001

    return (
        (var1
         - ((var2 * var3) / n))
        / abs(
            sqrt(var4 - ((var2 ** 2) / n))
            * sqrt(var5 - ((var3 ** 2) / n))
        )
    )


def similarity(persona_1_id, persona_2_id, medida):
    medidas_similitud = {
        'euclideana': euclidean,
        'manhattan': manhattan,
        'cosenoaprox': sim_coseno,
        'pearson': pearson
    }
    return medidas_similitud[medida](persona_1_id, persona_2_id)


def similarity_users(user_id, related_users, measure):

    target_person = User.objects.get(id=user_id)
    resultados = {}
    for person in related_users:
        resultados[person.slug] = similarity(
            target_person.id, person.id, measure)
        # if measure == 'pearson':
    return resultados
