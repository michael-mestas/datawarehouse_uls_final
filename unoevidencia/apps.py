from django.apps import AppConfig


class UnoevidenciaConfig(AppConfig):
    name = 'unoevidencia'
