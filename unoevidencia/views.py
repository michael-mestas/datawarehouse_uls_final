from django.shortcuts import render

from unoevidencia.models import User
from unoevidencia.utils import manhattan, euclidean, sim_coseno, pearson


def manhattan_view(request, persona_1_id, persona_2_id):

    if request.method == 'GET':

        context = {
            'suma': manhattan(persona_1_id, persona_2_id),
            'nombre_persona_1': User.objects.filter(
                id=persona_1_id).first().name,
            'nombre_persona_2': User.objects.filter(
                id=persona_2_id).first().name
        }
    return render(request, 'manhatan_2.html', context=context)


def euclidean_view(request, persona_1_id, persona_2_id):

    if request.method == 'GET':
        context = {
            'suma': euclidean(persona_1_id, persona_2_id),
            'nombre_persona_1': User.objects.filter(
                id=persona_1_id).first().name,
            'nombre_persona_2': User.objects.filter(
                id=persona_2_id).first().name
        }
        return render(request, 'euclidean_2.html', context=context)


def sim_coseno_view(request, persona_1_id, persona_2_id):

    if request.method == 'GET':
        context = {
            'suma': sim_coseno(persona_1_id, persona_2_id),
            'nombre_persona_1': User.objects.filter(
                id=persona_1_id).first().name,
            'nombre_persona_2': User.objects.filter(
                id=persona_2_id).first().name
        }
        return render(request, 'sim_coseno.html', context)


def pearson_view(request, persona_1_id, persona_2_id):

    if request.method == 'GET':
        context = {
            'suma': pearson(persona_1_id, persona_2_id),
            'nombre_persona_1': User.objects.filter(
                id=persona_1_id).first().name,
            'nombre_persona_2': User.objects.filter(
                id=persona_2_id).first().name
        }
        return render(request, 'pearson.html', context)


def k_vecinos_view(request, medida, persona_id, k):
    medidas_similitud={
        'euclideana': euclidean,
        'manhattan': manhattan,
    #     'coseno':,
        'cosenoaprox': sim_coseno,
        'pearson': pearson
    }
    target_person = User.objects.get(id=persona_id)
    resultados = []
    for person in User.objects.exclude(id=persona_id).iterator():
        resultados.append(
            (
                person.name,
                medidas_similitud[medida](target_person.id, person.id)
            )
        )

    resultados.sort(
        key=lambda x: x[1],
        reverse=False if medida != 'pearson' else True
    )
    context = {
        'target_name': target_person.name,
        'k': k,
        'resultados': resultados[:int(k)]
    }
    return render(request, 'k_vecinos.html', context)
