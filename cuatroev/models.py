from django.db import models

from unoevidencia.models import User, Item


class Distance(models.Model):
    centroid = models.ForeignKey(User, related_name='centroid')
    user = models.ForeignKey(User)
    distance = models.FloatField()
    is_historic = models.BooleanField(default=False)

    class Meta:
        unique_together = ('centroid', 'user', 'is_historic')


class Cluster(models.Model):
    user = models.ForeignKey(User)
    item = models.ForeignKey(Item)
