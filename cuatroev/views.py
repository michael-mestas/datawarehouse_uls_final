from django.shortcuts import render

from cuatroev.utils import KmeansGrouping, Kmeans


def kmeans_grouping_view(request, k, method_name):
    group1, group2 = KmeansGrouping(method_name).get_groups(k)
    context = {
        'group1': group1,
        'group2': group2
    }
    render(request, 'kmeans_grouping.html', context)

def kmeans_view(request, k, method_name):
    centroids, clusters = Kmeans().generate_k_means()
    context = {'centroids': centroids, 'clusters': clusters}
    render(request, 'TEMPLATE', context)
