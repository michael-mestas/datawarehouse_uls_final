import copy

from cuatroev.models import Distance, Cluster
from unoevidencia.models import Affinity, User, Item
from unoevidencia.utils import similarity


class Kmeans(object):
    """
    Class Kmeans encargada de realizar el algoritmo k-means, para utilizar esta
    clase se debe instanciar la clase con los siguientes parametros

    :param n_kmeans: Numero de centroides a utilizar
    :param similarity: Str que sacara el tipo de distancia que se utilizara
    :param n_loops: numero de veces que se hara el algoritmo, hasta cuantas
        vueltas dara y si no encuentra el equilibrio en los clusters, parara al
        encontrar este numero de vueltas

    kmeans = Kmeans(2, 'manhattan', 100)
    kmeans.generate_k_means()

    Por ultimo el algoritmo devuelve los centroides en los cuales encontro el
    equilibrio
    """

    options = ['euclideana', 'manhattan', 'cosenoaprox', 'pearson']
    old_cluster = {}
    cluster = {}

    def __init__(self, n_kmeans, similarity, n_loops):
        self.kmeans = n_kmeans
        self.n_loops = n_loops
        if similarity in self.options:
            self.similarity = similarity
        else:
            raise TypeError

    def cluster_groups_are_equal(self):
        for user in User.objects.exclude(is_centroid=True):
            distance = Distance.objects.filter(
                user=user, is_historic=True).order_by(
                '-distance').first().centroid
            h_distance = Distance.objects.filter(
                user=user, is_historic=False).order_by(
                '-distance').first().centroid
            if  distance != h_distance:
                return False
        return True

    def generate_k_means(self):
        self.pick_n_randoms()
        self.get_distances()
        self.get_clusters_groups()

        n_loops = 1
        while n_loops < self.n_loops:
            if self.cluster_groups_are_equal():
                break
            self.update_centroids()
            self.get_distances()
            self.get_clusters_groups()
        centroid_affinities = []
        for centroid in self.list_of_centroids:
            for item in Item.objects.all():
                centroid_affinities.append(Affinity.objects.filter(
                    user=centroid, item=item).first)
        return centroid_affinities, self.cluster

    def pick_n_randoms(self):

        list_of_randoms = []
        while len(list_of_randoms) <= self.kmeans:
            tmp_object = User.objects.order_by('?').first()
            if tmp_object not in list_of_randoms:
                list_of_randoms.append(tmp_object)
        list_of_randoms_to = []
        for random_index, random in enumerate(list_of_randoms, start=1):
            list_of_randoms_to.append(
                self.create_centroids(random_index, random))
        self.list_of_centroids = list_of_randoms_to

    def create_centroids(self, i, user):

        new_user = User(name=str(i), is_centroid=True).save()
        for affinity in Affinity.objects.filter(user=user):
            new_affinity = Affinity(
                user=new_user, item=affinity.item, rating=affinity.rating)
            new_affinity.save()
        return new_user

    def delete_centroids(self, i):
        user = User.objects.filter(id=str(i)).first()
        for affinity in Affinity.objects.filter(user=user):
            affinity.delete()

    def get_distances(self):
        Distance.objects.filter(is_historic=True).delete()
        Distance.objects.all().update_or_create(is_historic=True)
        for centroid in self.list_of_centroids:
            for user in self.User.objects.all():
                Distance(
                    centroid=centroid, user=user, distance=similarity(
                             centroid.id, user.id, self.similarity)
                ).save()

    def get_clusters_groups(self):
        clusters = {}
        self.old_cluster = copy.deepcopy(self.cluster)
        for centroid in self.list_of_centroids:
            clusters[centroid.name] = []

        for user in User.objects.all():
            distance = Distance.objects.filter(
                user=user).order_by('-distance').first()
            clusters[distance.centroid.name].append(distance)
        self.cluster = clusters

    def update_centroids(self):
        n_of_key = 1
        for key in self.cluster:
            for item in Item.objects.all():
                total_sum = 0
                total_users = 0
                for distance in self.cluster[key]:
                    total_sum += Affinity.objects.get(
                        user=distance.user, item=item).rating
                    total_users += 1
                centroid = User.objects.filter(name=str(n_of_key)).first()
                Affinity.objects.filter(user=centroid, item=item).update(
                    rating=(total_sum / total_users))


class KmeansGrouping(object):
    """
    agrupacion = KmeansGrouping('manhattan')
    grupo1, grupo2 = agrupacion.get_groups()
    """

    def __init__(self, similarity):
        self.similarity = similarity

    def create_matrix(self):
        for user in User.objects.all():
            for target_user in User.objects.exclude(user=user):
                if not Distance.objects.filter(
                        centroid=target_user, user=user).exists():
                    Distance(
                        centroid=user, user=target_user, distance=similarity(
                            user.id, target_user.id, self.similarity)).save()

    def get_lowest(self):
        n_groups = User.objects.all().count()
        self.distances = Distance.objects.all().order_by(
            '-distance')[:int(n_groups)]
        self.distances.order_by('distance')

    def get_groups(self, k):
        return self.distances[:k-1], self.distances[k:]