from django.conf.urls import url

from cuatroev.views import kmeans_view, kmeans_grouping_view
from dosevidencia.views import (
    cosine_adjusted_view, k_vecinos_view, prediction_view,
    generate_deviations_view, slope_one_view)

app_name = 'evcuatro'
urlpatterns = [
    url(r'^kmeans/(\d+)/(\w+)/', kmeans_view),

    url(r'^kmeansgrouping/(\d+)/(\w+)/', kmeans_grouping_view)
]