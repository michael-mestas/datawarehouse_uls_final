"""
Definition of urls for final_dw.
"""

from django.conf.urls import include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

from dosevidencia.views import home

admin.autodiscover()


urlpatterns = [
    # Examples:
    url(r'^$', home, name='home'),
    url(r'^evuno/', include('unoevidencia.urls')),
    url(r'^m/', include('filemigration.urls')),
    url(r'^evdos/', include('dosevidencia.urls')),
    url(r'^evcuatro/', include('cuatroev.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

]
