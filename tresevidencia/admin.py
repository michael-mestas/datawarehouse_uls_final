from django.contrib import admin

from tresevidencia.models import RawCommon, Classification, RawAffinity


class RawAffinityAdmin(admin.ModelAdmin):
    list_display = ('user', 'item', 'rating')


class RawCommonAdmin(admin.ModelAdmin):
    list_display = ('medium', 'total', 'total_sum', 'item')


class ClassificationAdmin(admin.ModelAdmin):
    list_display = ('user', 'name')


admin.site.register(RawAffinity, RawAffinityAdmin)
admin.site.register(RawCommon, RawCommonAdmin)
admin.site.register(Classification, ClassificationAdmin)


