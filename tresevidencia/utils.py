import csv

from tresevidencia.models import RawAffinity, RawCommon, Classification
from unoevidencia.models import Affinity, Item, User


class CleanData(object):

    def __init__(self):

        for item in Item.objects.all():
            self.set_medium(item)
            self.normalize(item)

    def set_medium(self, item):

        value_list = []
        raw_afinities = RawAffinity.objects.filter(item=item)
        total_sum = 0
        for affinity in raw_afinities:
            value_list.append(affinity.rating)
            value_list.sort()
        medium = value_list[round(len(raw_afinities) / 2) - 1] if (
            len(value_list) % 2 == 1) else (
            (
                value_list[round(len(raw_afinities) / 2) - 1]
                + value_list[round(len(raw_afinities) / 2)]
            ) / 2
        )
        for affinity in raw_afinities:
            total_sum += abs(affinity.rating - medium)
        RawCommon(
            medium=medium, total_sum=total_sum, total=raw_afinities.count(),
            item=item).save()

    def normalize(self, item):

        common_values = RawCommon.objects.get(item=item)
        denominator = ((1 / common_values.total) * common_values.total_sum)
        for affinity in RawAffinity.objects.filter(item=item):
            rating = (affinity.rating - common_values.medium) / denominator
            Affinity(
                user=affinity.user, item=affinity.item, rating=rating).save()


def normalize_single_object(raw_item):
    common_values = RawCommon.objects.get(item=raw_item.item)
    nominator = raw_item.rating - common_values.medium
    denominator = 1/common_values.total * common_values.total_sum


class LinearMigration(object):
    path = (
        'C:/Users/michael/Desktop/Universidad La Salle/X/dw 2/databases/'
        'ev3/copies/')
    file_name = {
        'athletes': 'athletesTestSet.txt',
        'iris': 'irisTestSet.data.txt',
        'mpg': 'mpgTestSet.txt'
    }

    user = None
    items_groups = {
        'athletes': (2, 3),
        'iris': (0, 1, 2, 3),
        'mpg': (1, 2, 3, 4, 5)
    }

    def __init__(self, database_name):

        if database_name in self.file_name:
            self.database_name = database_name
        else:
            raise AttributeError

    def migrate(self):
        method_name = {
            'athletes': self.athletes_migrate,
            'iris': self.iris_migrate,
            'mpg': self.mpg_migrate
        }

        path = '{}{}'.format(self.path, self.file_name[self.database_name])
        with open(path, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
            i = 0
            for row in spamreader:
                j = 0
                for element in row:
                    if i == 0:
                        pass
                    else:
                        method_name[self.database_name](element, j)
                    j += 1
                i += 1

    def athletes_migrate(self, data, index):

        if index == 0:
            obj, _ = User.objects.update_or_create(name=data, slug=data)
            self.user = User.objects.get(slug=data)
        else:
            if index == 1:
                Classification.objects.update_or_create(
                    name=data,
                    user=self.user)
            else:
                RawAffinity(
                    user=self.user, rating=data,
                    item=Item.objects.get(slug=str(index))).save()



    def iris_migrate(self, data):
        print("2222222222222222222222222")
        print(data)

    def mpg_migrate(self, data):
        print("3333333333333333333333333")
        print(data)
