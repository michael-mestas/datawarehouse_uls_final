from django.db import models

from unoevidencia.models import User, Item


class RawAffinity(models.Model):

    user = models.ForeignKey(User)
    item = models.ForeignKey(Item)
    rating = models.FloatField(null=True, blank=True)

    def __str__(self):
        return "{var_1} + {var_2}".format(var_1=self.user, var_2=self.item)


class RawCommon(models.Model):
    medium = models.FloatField()
    total = models.IntegerField()
    total_sum = models.FloatField()
    item = models.OneToOneField(Item)


class Classification(models.Model):
    name = models.CharField(max_length=32)
    user = models.ForeignKey(User)