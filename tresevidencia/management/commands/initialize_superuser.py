from django.contrib.auth.models import User
from django.core.management import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):

        # user, _ = User.objects.get(username='admin').delete()
        user, _ = User.objects.update_or_create(
            username='admin', is_staff=True, is_active=True, is_superuser=True)

        user.set_password('a123456789B!')
        user.save()
