from django.apps import AppConfig


class TresevidenciaConfig(AppConfig):
    name = 'tresevidencia'
