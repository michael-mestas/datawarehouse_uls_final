from django.shortcuts import render
from tresevidencia.io_migrations import *
# Create your views here.

def migrate_view(request):
    Migrate().migrate()
    return render(request, 'migration.html', context={})