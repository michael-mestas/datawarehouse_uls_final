from django.shortcuts import render

from .utils import MatrixMigration, LinearMigration
from tresevidencia.utils import CleanData


def migrate_w_view(request, name):
    # LinearMigration(name).initialize_variables()
    # LinearMigration(name).migrate()
    CleanData()
    return render(request, 'migration.html', {})


def migrate_view(request):

    MatrixMigration().migrate()
    return render(request, 'migration.html', {})
