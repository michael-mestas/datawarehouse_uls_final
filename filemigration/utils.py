import csv

from tresevidencia.models import Classification, RawAffinity
from unoevidencia.models import Affinity, User, Item


class LinearMigration(object):
    path = (
        'C:/Users/michael/Desktop/Universidad La Salle/X/dw 2/databases/'
        'ev3/copies/')
    file_name = {
        'athletes': 'athletesTrainingSet.txt',
        'iris': 'irisTrainingSet.data.txt',
        'mpg': 'mpgTrainingSet.txt'
    }

    user = None
    items_groups = {
        'athletes': (2, 3),
        'iris': (0, 1, 2, 3),
        'mpg': (1, 2, 3, 4, 5)
    }

    def __init__(self, database_name):

        if database_name in self.file_name:
            self.database_name = database_name
        else:
            raise AttributeError

    def initialize_variables(self):
        for item_name in self.items_groups[self.database_name]:
            Item.objects.update_or_create(
                name=str(item_name), slug=str(item_name))


    def migrate(self):
        method_name = {
            'athletes': self.athletes_migrate,
            'iris': self.iris_migrate,
            'mpg': self.mpg_migrate
        }

        path = '{}{}'.format(self.path, self.file_name[self.database_name])
        with open(path, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
            i = 0
            for row in spamreader:
                j = 0
                for element in row:
                    if i == 0:
                        pass
                    else:
                        method_name[self.database_name](element, j)
                    j += 1
                i += 1

    def athletes_migrate(self, data, index):

        if index == 0:
            obj, _ = User.objects.update_or_create(name=data, slug=data)
            self.user = User.objects.get(slug=data)
        else:
            if index == 1:
                Classification.objects.update_or_create(
                    name=data,
                    user=self.user)
            else:
                RawAffinity(
                    user=self.user, rating=data,
                    item=Item.objects.get(slug=str(index))).save()



    def iris_migrate(self, data):
        print("2222222222222222222222222")
        print(data)

    def mpg_migrate(self, data):
        print("3333333333333333333333333")
        print(data)


class MatrixMigration:
    def __init__(self):
        self.path = (
            "C:/Users/michael/Desktop/Universidad La Salle/X/dw 2/databases/"
            "Movie_Ratings.csv")

    def migrate(self):
        with open(self.path, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            i = 0
            usuarios = []
            item_actual = None
            for row in spamreader:
                j = 0
                for element in row:
                    if element == '':
                        pass
                    else:
                        if i == 0:
                            User(name=element, slug=element).save()
                            usuarios.append(User.objects.get(slug=element))

                        else:
                            if j == 0:
                                Item(name=element, slug=element).save()
                                item_actual = Item.objects.get(slug=element)

                            else:
                                Affinity(user=usuarios[j-1], item=item_actual,
                                         rating=element).save()

                    j += 1
                i += 1


class MatrixMigrate(object):

    path='C:/Users/michael/Desktop/Universidad La Salle/X/dw 2/databases/Movie_Ratings.csv'

    def __init__(self, path=None):
        if path==None:
            pass

    def save_header(self, header):
        for name in header:
            if name != '':
                User.objects.update_or_create(name=name, slug=name)
        return header

    def migrate(self):
        with open(self.path, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

            i = 0
            column_header = []
            for row in spamreader:
                j = 0
                for element in row:
                    if i == 0:
                        header = self.save_header(row)
                        i += 1

                    if j == 0:
                        Item.objects.update_or_create(name=element, slug=element)
                        column_header.append(element)
                        j += 1

                    else:
                        if element != '':
                            Affinity(
                                rating=element,
                                user=User.objects.get(
                                    slug=header[row.index(element)]),
                                item=Item.objects.get(
                                    slug=column_header[spamreader.index(row)])
                            ).save()
                i += 1

def add_test():
    #load files

    lista_items = []
    for item in Item.objects.all():
        lista_items.append((1, item))
    # comenzaraleer

