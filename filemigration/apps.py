from django.apps import AppConfig


class FilemigrationConfig(AppConfig):
    name = 'filemigration'
