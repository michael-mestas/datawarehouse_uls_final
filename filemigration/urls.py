from django.conf.urls import url

from filemigration.views import migrate_view, migrate_w_view

urlpatterns = [
    url(r'^migrate/(\w+)/', migrate_w_view),
    url(r'^migrate/', migrate_view)
]